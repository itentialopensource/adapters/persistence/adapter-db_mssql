# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the MicrosoftSQL System. The API that was used to build the adapter for MicrosoftSQL is usually available in the report directory of this adapter. The adapter utilizes the MicrosoftSQL API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The MSSQL adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Microsoft MSSQL. With this adapter you have the ability to perform operations with MSSQL on items such as:

- Storage of Information
- Retrieval of Information

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
