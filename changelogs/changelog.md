
## 1.0.0 [01-05-2024]

* Adapter Migration

See merge request itentialopensource/adapters/persistence/adapter-db_mssql!5

---

## 0.4.0 [07-16-2021]

* added call that removes the record set from select returns

See merge request itentialopensource/adapters/persistence/adapter-db_mssql!3

---

## 0.3.0 [03-26-2021]

* add domain support for connecting

See merge request itentialopensource/adapters/persistence/adapter-db_mssql!2

---

## 0.2.0 [08-04-2020]

* add encrypt capability to the connextion

See merge request itentialopensource/adapters/persistence/adapter-db_mssql!1

---

## 0.1.1 [05-20-2020]

* Bug fixes and performance improvements

See commit f5ac952

---
