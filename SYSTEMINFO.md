# Microsoft SQL

Vendor: Microsoft
Homepage: https://www.microsoft.com/en-us/

Product: MSSQL
Product Page: https://learn.microsoft.com/en-us/sql/?view=sql-server-ver16

## Introduction
We classify MSSQL into the Data Storage domaina as MSSQL is a database which provides the storage of information. 

"Visibility over your entire data estate"
"The most secure database over the last 10 years"
"Industry-leading performance and availability"

## Why Integrate
The MSSQL adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Microsoft MSSQL. With this adapter you have the ability to perform operations with MSSQL on items such as:

- Storage of Information
- Retrieval of Information

## Additional Product Documentation
The [MSSQL Node Library Documentation](https://www.npmjs.com/package/mssql)