
## 1.0.6 [10-15-2024]

* Changes made at 2024.10.14_20:57PM

See merge request itentialopensource/adapters/adapter-db_mssql!16

---

## 1.0.5 [08-25-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-db_mssql!14

---

## 1.0.4 [08-14-2024]

* Changes made at 2024.08.14_19:17PM

See merge request itentialopensource/adapters/adapter-db_mssql!13

---

## 1.0.3 [08-07-2024]

* Changes made at 2024.08.07_10:51AM

See merge request itentialopensource/adapters/adapter-db_mssql!12

---

## 1.0.2 [07-26-2024]

* manual migration updates

See merge request itentialopensource/adapters/persistence/adapter-db_mssql!10

---

## 1.0.1 [01-05-2024]

* more migration changes

See merge request itentialopensource/adapters/persistence/adapter-db_mssql!6

---

## 1.0.0 [01-05-2024]

* Adapter Migration

See merge request itentialopensource/adapters/persistence/adapter-db_mssql!5

---

## 0.4.0 [07-16-2021]

* added call that removes the record set from select returns

See merge request itentialopensource/adapters/persistence/adapter-db_mssql!3

---

## 0.3.0 [03-26-2021]

* add domain support for connecting

See merge request itentialopensource/adapters/persistence/adapter-db_mssql!2

---

## 0.2.0 [08-04-2020]

* add encrypt capability to the connextion

See merge request itentialopensource/adapters/persistence/adapter-db_mssql!1

---

## 0.1.1 [05-20-2020]

* Bug fixes and performance improvements

See commit f5ac952

---
